package CloudIt.Africa.InterviewPractical.View;



import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.ModelAttribute;

@RestController
public class ControllerRest {

	@RequestMapping("/UserUpdate/{userId}")
	public User getUserProfilePage(@PathVariable("userId") Integer userId, @RequestBody User user) {
		DataRepository datarepo = new DataRepository();
		User usernew = datarepo.getUser(userId);

		return usernew;
	}

	@PostMapping("/login")
	public ModelAndView  loginSubmit(ModelMap model,@ModelAttribute User user, RedirectAttributes ra) {
		DataRepository data = new DataRepository();
		User _user = data.findByEmail(user.getEmail());
		
		if(_user == null){
			ra.addFlashAttribute("message", "User does not exist");
			return new ModelAndView("redirect:/Login");
		}else{
			ra.addFlashAttribute("user",_user);
			return new ModelAndView("redirect:/Dashboard");
		}

		// JsonResponse res = new JsonResponse();
		// HashMap<String, Object> map = new HashMap<>();
		// if (user == null) {
		// 	// res.setStatus("FAIL");
		// 	// map.put("msg", "User does not exist");
		// 	// res.setResult(map);
		// 	model.addAttribute("error", "User does not exist");
		// 	model.addAttribute("login", "User does not exist");
		// 	return (T) new ModelAndView("redirect:/Login", model);
		// 	// return (T) res;
		// } else {
		// 	// res.setStatus("SUCCESS");
		// 	// map.put("msg", "Login Successfull");
		// 	// res.setResult(map);
		// 	model.addAttribute("user", "redirectWithRedirectPrefix");
		// 	return (T) new ModelAndView("redirect:/Dashboard", model);
		// }
	}

}
