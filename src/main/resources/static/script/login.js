/**
 * INDEX PAGE JAVASCRIPT
 */

function login(){
    var formData = {
       email: $('#email').val(),
       password: $('#password').val()
    } ;
    console.log(formData);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/login",
        data: JSON.stringify(formData),
        cache:false,
        success: (response)=>{
            console.log(response);
            if(response.status == 'FAIL'){
                M.toast({html: response.result.msg});
            }else if(response.status == 'SUCCESS'){
                window.location = '/Dashboard'
            }else{
                // window.location = response.url
            }
        },
        error:(error)=>{
            console.log(error);
        }
    });
}